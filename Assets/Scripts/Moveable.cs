﻿using UnityEngine;
using System.Collections;

public class Moveable : MonoBehaviour {
    public bool gravityStartsEnabled = true;
    public bool gravityAfterMove = true;
    
	void Start () {
        if (GetComponent<Collider>() == null)
            gameObject.AddComponent<BoxCollider>();
        if (GetComponent<Rigidbody>() == null)
            gameObject.AddComponent<Rigidbody>();

        Rigidbody rb = GetComponent<Rigidbody>();
        rb.useGravity = gravityStartsEnabled;
	}
	
}
