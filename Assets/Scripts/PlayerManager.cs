﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerManager : MonoBehaviour
{
    //this allows the creation of an oculus rift player prefab. automatically defaults to
    //normal computer screen when in unity editor.
    public static bool isOculus = false;
    public static bool madePlayer = false;

    //this is the gameObject whose transform.forward is the forward of the camera.
    //did not simply use Camera.main.transform.forward for oculus rift compatibility.
    private static GameObject forwardGameObject;

    void SetPlayer()
    {
        GameObject player = Instantiate(GetPlayerPrefab()) as GameObject;
        player.transform.position = transform.position;
        player.transform.rotation = transform.rotation;
    }

    public GameObject GetPlayerPrefab()
    {
        if (IsOculus())
            return Resources.Load("Prefabs/oculus_player") as GameObject;
        else
            return Resources.Load("Prefabs/player") as GameObject;
    }

    public static bool IsOculus()
    {
        //because there's no damn way to detect if an OR is plugged in?
        return isOculus;
    }

    public static Vector3 GetCameraPosition()
    {
        return GetCameraGameObject().transform.position;
    }

    public static Vector3 GetCameraForward()
    {
        return GetCameraGameObject().transform.forward;
    }

    public static GameObject GetCameraGameObject()
    {
        if (forwardGameObject == null)
        {
            if (PlayerManager.IsOculus())
                forwardGameObject = GameObject.Find("RightEyeAnchor") as GameObject;
            else
                forwardGameObject = Camera.main.gameObject;
        }
        return forwardGameObject;
    }

    void Update()
    {
        if (!madePlayer)
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                isOculus = true;
                madePlayer = true;
                SetPlayer();
            }
            else if (Input.GetKeyDown(KeyCode.P) || Application.isEditor)
            {
                isOculus = false;
                madePlayer = true;
                SetPlayer();
            }
        }

    }

}
