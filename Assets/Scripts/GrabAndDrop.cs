﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GrabAndDrop : MonoBehaviour
{
    static List<Collider> allColliders;
    GameObject movedContainer;
    GameObject grabbedObject;
    float grabbedDistance;
    Vector3 grabbedScale;

    //how far in meters you can grab and drop objects
    private const float MAX_GRAB_DISTANCE = 1000;

    //this is how close a forced perspective will get to its ideal position.
    //example: if ideal scale/position is 97.152%, then it will position at 98% or 96%.
    private const float SLIDE_PERCENT = 0.02f;
    //no object's render bounds will be made smaller than this.
    private const float MIN_RENDER_BOUNDS_SQMAG = 0.05f;

    void Start()
    {
        GetAllColliders();
        movedContainer = new GameObject("moved objects");
    }

    GameObject GetMouseHoverObject(float range)
    {
        //returns the object in the center of the screen, at most 'range' meters away from player.
        Vector3 position = PlayerManager.GetCameraPosition();
        RaycastHit rayCastHit;
        Vector3 target = position + PlayerManager.GetCameraForward() * range;
        if (Physics.Linecast(position, target, out rayCastHit))
            return rayCastHit.collider.gameObject;
        return null;
    }

    void TryGrabObject(GameObject grabObject)
    {
        if (grabObject == null || !CanGrab(grabObject))
            return;
        grabbedObject = grabObject;
        grabbedDistance = Vector3.Distance(PlayerManager.GetCameraPosition(), grabObject.transform.position);
        grabbedScale = grabbedObject.transform.localScale;
        grabbedObject.layer = LayerMask.NameToLayer("GrabObject"); ;
        SetObjectFreeze(grabbedObject, true);
    }

    bool CanGrab(GameObject candidate)
    {
        return candidate.GetComponent<Moveable>() != null;
    }

    void SetObjectFreeze(GameObject go, bool isFrozen)
    {
        //this freezes the physics of the object, and sets its mass according to its size
        Rigidbody rb = go.GetComponent<Rigidbody>();
        Moveable m = go.GetComponent<Moveable>();
        go.GetComponent<Collider>().enabled = !isFrozen;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.useGravity = !isFrozen && m.gravityAfterMove;
        rb.freezeRotation = isFrozen;
        rb.mass = go.GetComponent<Renderer>().bounds.size.sqrMagnitude;
    }

    void DropObject()
    {
        SetObjectFreeze(grabbedObject, false);

        RaycastHit rayCastHit = GetRayHitBehindGrabbedObject(PlayerManager.GetCameraPosition(), PlayerManager.GetCameraForward(), grabbedObject);
        if (rayCastHit.collider == null)
        {
            //no objects are behind grabbed object
            grabbedObject.transform.position = PlayerManager.GetCameraPosition() + PlayerManager.GetCameraForward() * grabbedDistance;
            grabbedObject.transform.localScale = grabbedScale;
        }
        else
        {
            Vector3 wallPoint = rayCastHit.point;
            ForcePerspectiveOnGrabbed(wallPoint);
        }
        //reset its layer to 1 like everything else.
        grabbedObject.layer = 1;

        grabbedObject.transform.parent = movedContainer.transform;
        grabbedObject = null;
    }

    public static RaycastHit GetRayHitBehindGrabbedObject(Vector3 startPosition, Vector3 lookDirection, GameObject ignoreObject)
    {
        //generic testable method
        //performs a raycast and shoots through the grabbed object.
        RaycastHit hitInfo = new RaycastHit();
        int layer = 1;
        if (Physics.Raycast(startPosition, lookDirection, out hitInfo, MAX_GRAB_DISTANCE, layer))
            return hitInfo;
        Debug.Log("GetRayHitBehindObject hit no object. " + startPosition + " " + lookDirection + " " + ignoreObject);
        return hitInfo;
    }

    private static List<Collider> GetAllColliders()
    {
        if (allColliders == null)
        {
            allColliders = new List<Collider>();
            foreach (Collider c in GameObject.FindObjectsOfType<Collider>())
                if (c.gameObject.GetComponent<Crosshair>() == null && c.gameObject.GetComponent<Terrain>() == null)
                    allColliders.Add(c);
        }
        return allColliders;
    }

    private static bool IsColliding(GameObject go)
    {
        //returns true if "go" is colliding with anything.
        Collider objectCollider = go.GetComponent<Collider>();
        foreach (Collider c in GetAllColliders())
        {
            if (c.gameObject == go)
                continue;
            if (objectCollider.GetComponent<Collider>().bounds.Intersects(c.gameObject.GetComponent<Collider>().bounds))
                return true;
        }
        return false;
    }

    public static void ForcePerspective(GameObject camera, GameObject grabbed, float startDistance, Vector3 startScale, Vector3 wallPoint)
    {
        //generic testable method

        //camera is a game object for player position and transform.forward
        //grabbed is the object we want to force perspective on.
        //startDistance is the distance from the camera to the object when it was first grabbed.
        //startScale is the scale of the object when it was first grabbed.
        //wallPoint is the farthest point (and largest scale) from the camera which the object will go to.

        float maxDistance = Vector3.Distance(camera.transform.position, wallPoint);
        float ratio = maxDistance / startDistance;
        Vector3 maxScale = startScale * ratio;
        Vector3 cameraPosition = camera.transform.position;

        //I think this optimizes...
        Collider grabbedCollider = grabbed.GetComponent<Collider>();

        //starting at the largest possible scale and farthest point from camera, slowly shrink the object and 
        //move it closer to camera until it collides with nothing.
        bool foundNoCollide = false;
        for (float i = 1; i > 0; i -= SLIDE_PERCENT)
        {
            Vector3 newScale = Vector3.Lerp(Vector3.zero, maxScale, i);
            Vector3 newPosition = Vector3.Lerp(cameraPosition, wallPoint, i);
            grabbed.transform.localScale = newScale;
            grabbed.transform.position = newPosition;

            if (newScale.sqrMagnitude < MIN_RENDER_BOUNDS_SQMAG)
                break;

            if (!IsColliding(grabbed))
            {
                foundNoCollide = true;
                break;
            }
        }

        if (foundNoCollide)
        {
            //this seems to be necessary to update allColliders with the new large collider.
            allColliders.Remove(grabbed.GetComponent<Collider>());
            allColliders.Add(grabbed.GetComponent<Collider>());
        }
        else
        {
            Debug.Log("ForcePerspective couldn't find a non-collide spot. Setting scale to same as when picked up.");
            grabbed.transform.position = camera.transform.position + camera.transform.forward * startDistance;
            Debug.Log("cam.pos=" + camera.transform.position + " cam.for=" + camera.transform.forward + " startD=" + startDistance + " grab.pos=" + grabbed.transform.position);
            grabbed.transform.localScale = startScale;
        }
    }

    void ForcePerspectiveOnGrabbed(Vector3 wallPoint)
    {
        //ForcePerspective is generic (and testable), this method uses it specifically for the game.
        ForcePerspective(PlayerManager.GetCameraGameObject(), grabbedObject, grabbedDistance, grabbedScale, wallPoint);
    }

    void CreateCube(Vector3 position)
    {
        //method for debugging. basically a gizmo.
        GameObject prefab = Resources.Load("Prefabs/debug-cube") as GameObject;
        GameObject cube = Instantiate(prefab) as GameObject;
        cube.transform.position = position;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (grabbedObject == null)
                TryGrabObject(GetMouseHoverObject(MAX_GRAB_DISTANCE));
            else
                DropObject();
        }

        if (grabbedObject != null)
        {
            Vector3 newPosition = PlayerManager.GetCameraPosition() + PlayerManager.GetCameraForward() * grabbedDistance;
            grabbedObject.transform.position = newPosition;
        }
    }
}
