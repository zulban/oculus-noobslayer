﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour
{
    GameObject crosshair;

    void Start()
    {
        GameObject prefab = Resources.Load("Prefabs/crosshair") as GameObject;
        crosshair = Instantiate(prefab) as GameObject;
        crosshair.transform.parent = transform;
    }

    void Update()
    {
        crosshair.transform.position = PlayerManager.GetCameraPosition() + PlayerManager.GetCameraForward();
        crosshair.transform.LookAt(PlayerManager.GetCameraPosition());
    }
}
