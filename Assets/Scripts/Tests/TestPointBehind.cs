﻿using UnityEngine;
using System.Collections;

public class TestPointBehind : MonoBehaviour
{
    public GameObject cameraGameObject = null;
    public GameObject ignoreObject = null;
    public GameObject expectedBehindObject = null;

    void Start()
    {
        ignoreObject.layer = LayerMask.NameToLayer("GrabObject");
        RaycastHit ray = GrabAndDrop.GetRayHitBehindGrabbedObject(cameraGameObject.transform.position,
            cameraGameObject.transform.forward, ignoreObject);

        if (ray.collider == null)
        {
            string message = gameObject.name;
            UnitTest.AssertEqual(message, expectedBehindObject, null);
        }
        else
        {
            GameObject result = ray.collider.gameObject;
            string message = gameObject.name + " expected: " + expectedBehindObject.name + " result: " + result.name;
            UnitTest.AssertEqual(message, expectedBehindObject, result);
        }
    }

}
