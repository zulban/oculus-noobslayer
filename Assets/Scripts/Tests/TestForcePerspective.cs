﻿using UnityEngine;
using System.Collections;

public class TestForcePerspective : MonoBehaviour
{
    public GameObject cameraGameObject;
    public GameObject grabbedObject;
    public GameObject wallPointGameObject;

    void Start()
    {
        float startDistance=Vector3.Distance(grabbedObject.transform.position,cameraGameObject.transform.position);
        Vector3 startScale=grabbedObject.transform.localScale;
        GrabAndDrop.ForcePerspective(cameraGameObject, grabbedObject, startDistance, 
            startScale, wallPointGameObject.transform.position);
    }
}
