﻿using UnityEngine;
using System.Collections;

public static class UnitTest  {

    public static void AssertEqual(string message, GameObject go1, GameObject go2)
    {
        if (go1 == null && go2 == null)
            return;
        if (!go1.Equals(go2))
            Debug.LogError("AssertEqual failed - " + message +" '"+go1+"'  '"+go2+"'");
    }
}
