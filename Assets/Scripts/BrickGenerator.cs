﻿using UnityEngine;
using System.Collections;

public class BrickGenerator : MonoBehaviour {
	void Start () {
        GameObject container = new GameObject("bricks");
	
		GameObject prefab = Resources.Load ("Prefabs/brick") as GameObject;

		for (int i=0; i<25; i++) {
			for (int height=0;height<15;height++){

				GameObject brick=Instantiate(prefab) as GameObject;
                brick.transform.parent = container.transform;

				float offset=height%2;

				brick.transform.position=new Vector3(transform.position.x+i*2+offset,
				                                     transform.position.y+height+0.5f,
				                                     transform.position.z);

			}
				}
	}
}
